/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of Qt Creator.
**
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 as published by the Free Software
** Foundation with exceptions as appearing in the file LICENSE.GPL3-EXCEPT
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
****************************************************************************/

#pragma once

#include <QtGlobal>

namespace Core {
namespace Constants {

// Modes
const char MODE_WELCOME[]          = "Welcome";
const char MODE_EDIT[]             = "Edit";
const char MODE_DESIGN[]           = "Design";
const int  P_MODE_WELCOME          = 100;
const int  P_MODE_EDIT             = 90;
const int  P_MODE_DESIGN           = 89;

// Menubar
const char MENU_BAR[]              = "QTester.MenuBar";

// Menus
const char M_FILE[]                = "QTester.Menu.File";
const char M_FILE_RECENTFILES[]    = "QTester.Menu.File.RecentFiles";
const char M_EDIT[]                = "QTester.Menu.Edit";
const char M_EDIT_ADVANCED[]       = "QTester.Menu.Edit.Advanced";
const char M_TOOLS[]               = "QTester.Menu.Tools";
const char M_TOOLS_EXTERNAL[]      = "QTester.Menu.Tools.External";
const char M_WINDOW[]              = "QTester.Menu.Window";
const char M_WINDOW_PANES[]        = "QTester.Menu.Window.Panes";
const char M_WINDOW_VIEWS[]        = "QTester.Menu.Window.Views";
const char M_HELP[]                = "QTester.Menu.Help";

// Contexts
const char C_GLOBAL[]              = "Global Context";
const char C_WELCOME_MODE[]        = "Core.WelcomeMode";
const char C_EDIT_MODE[]           = "Core.EditMode";
const char C_DESIGN_MODE[]         = "Core.DesignMode";
const char C_EDITORMANAGER[]       = "Core.EditorManager";
const char C_NAVIGATION_PANE[]     = "Core.NavigationPane";
const char C_PROBLEM_PANE[]        = "Core.ProblemPane";
const char C_GENERAL_OUTPUT_PANE[] = "Core.GeneralOutputPane";

// Default editor kind
const char K_DEFAULT_TEXT_EDITOR_DISPLAY_NAME[] = QT_TRANSLATE_NOOP("OpenWith::Editors", "Plain Text Editor");
const char K_DEFAULT_TEXT_EDITOR_ID[] = "Core.PlainTextEditor";
const char K_DEFAULT_BINARY_EDITOR_ID[] = "Core.BinaryEditor";

//actions
const char UNDO[]                  = "QTester.Undo";
const char REDO[]                  = "QTester.Redo";
const char COPY[]                  = "QTester.Copy";
const char PASTE[]                 = "QTester.Paste";
const char CUT[]                   = "QTester.Cut";
const char SELECTALL[]             = "QTester.SelectAll";

const char GOTO[]                  = "QTester.Goto";
const char ZOOM_IN[]               = "QTester.ZoomIn";
const char ZOOM_OUT[]              = "QTester.ZoomOut";
const char ZOOM_RESET[]            = "QTester.ZoomReset";

const char NEW[]                   = "QTester.New";
const char OPEN[]                  = "QTester.Open";
const char OPEN_WITH[]             = "QTester.OpenWith";
const char REVERTTOSAVED[]         = "QTester.RevertToSaved";
const char SAVE[]                  = "QTester.Save";
const char SAVEAS[]                = "QTester.SaveAs";
const char SAVEALL[]               = "QTester.SaveAll";
const char PRINT[]                 = "QTester.Print";
const char EXIT[]                  = "QTester.Exit";

const char OPTIONS[]               = "QTester.Options";
const char TOGGLE_LEFT_SIDEBAR[]   = "QTester.ToggleLeftSidebar";
const char TOGGLE_RIGHT_SIDEBAR[]  = "QTester.ToggleRightSidebar";
const char TOGGLE_MODE_SELECTOR[]  = "QTester.ToggleModeSelector";
const char TOGGLE_FULLSCREEN[]     = "QTester.ToggleFullScreen";
const char THEMEOPTIONS[]          = "QTester.ThemeOptions";

const char TR_SHOW_LEFT_SIDEBAR[]  = QT_TRANSLATE_NOOP("Core", "Show Left Sidebar");
const char TR_HIDE_LEFT_SIDEBAR[]  = QT_TRANSLATE_NOOP("Core", "Hide Left Sidebar");

const char TR_SHOW_RIGHT_SIDEBAR[] = QT_TRANSLATE_NOOP("Core", "Show Right Sidebar");
const char TR_HIDE_RIGHT_SIDEBAR[] = QT_TRANSLATE_NOOP("Core", "Hide Right Sidebar");

const char MINIMIZE_WINDOW[]       = "QTester.MinimizeWindow";
const char ZOOM_WINDOW[]           = "QTester.ZoomWindow";
const char CLOSE_WINDOW[]           = "QTester.CloseWindow";

const char SPLIT[]                 = "QTester.Split";
const char SPLIT_SIDE_BY_SIDE[]    = "QTester.SplitSideBySide";
const char SPLIT_NEW_WINDOW[]      = "QTester.SplitNewWindow";
const char REMOVE_CURRENT_SPLIT[]  = "QTester.RemoveCurrentSplit";
const char REMOVE_ALL_SPLITS[]     = "QTester.RemoveAllSplits";
const char GOTO_PREV_SPLIT[]       = "QTester.GoToPreviousSplit";
const char GOTO_NEXT_SPLIT[]       = "QTester.GoToNextSplit";
const char CLOSE[]                 = "QTester.Close";
const char CLOSE_ALTERNATIVE[]     = "QTester.Close_Alternative"; // temporary, see QTesterBUG-72
const char CLOSEALL[]              = "QTester.CloseAll";
const char CLOSEOTHERS[]           = "QTester.CloseOthers";
const char CLOSEALLEXCEPTVISIBLE[] = "QTester.CloseAllExceptVisible";
const char GOTONEXT[]              = "QTester.GotoNext";
const char GOTOPREV[]              = "QTester.GotoPrevious";
const char GOTONEXTINHISTORY[]     = "QTester.GotoNextInHistory";
const char GOTOPREVINHISTORY[]     = "QTester.GotoPreviousInHistory";
const char GO_BACK[]               = "QTester.GoBack";
const char GO_FORWARD[]            = "QTester.GoForward";
const char ABOUT_QTESTER[]       = "QTester.AboutQTester";
const char ABOUT_PLUGINS[]         = "QTester.AboutPlugins";
const char S_RETURNTOEDITOR[]      = "QTester.ReturnToEditor";

// Default groups
const char G_DEFAULT_ONE[]         = "QTester.Group.Default.One";
const char G_DEFAULT_TWO[]         = "QTester.Group.Default.Two";
const char G_DEFAULT_THREE[]       = "QTester.Group.Default.Three";

// Main menu bar groups
const char G_FILE[]                = "QTester.Group.File";
const char G_EDIT[]                = "QTester.Group.Edit";
const char G_VIEW[]                = "QTester.Group.View";
const char G_TOOLS[]               = "QTester.Group.Tools";
const char G_WINDOW[]              = "QTester.Group.Window";
const char G_HELP[]                = "QTester.Group.Help";

// File menu groups
const char G_FILE_NEW[]            = "QTester.Group.File.New";
const char G_FILE_OPEN[]           = "QTester.Group.File.Open";
const char G_FILE_PROJECT[]        = "QTester.Group.File.Project";
const char G_FILE_SAVE[]           = "QTester.Group.File.Save";
const char G_FILE_EXPORT[]         = "QTester.Group.File.Export";
const char G_FILE_CLOSE[]          = "QTester.Group.File.Close";
const char G_FILE_PRINT[]          = "QTester.Group.File.Print";
const char G_FILE_OTHER[]          = "QTester.Group.File.Other";

// Edit menu groups
const char G_EDIT_UNDOREDO[]       = "QTester.Group.Edit.UndoRedo";
const char G_EDIT_COPYPASTE[]      = "QTester.Group.Edit.CopyPaste";
const char G_EDIT_SELECTALL[]      = "QTester.Group.Edit.SelectAll";
const char G_EDIT_ADVANCED[]       = "QTester.Group.Edit.Advanced";

const char G_EDIT_FIND[]           = "QTester.Group.Edit.Find";
const char G_EDIT_OTHER[]          = "QTester.Group.Edit.Other";

// Advanced edit menu groups
const char G_EDIT_FORMAT[]         = "QTester.Group.Edit.Format";
const char G_EDIT_COLLAPSING[]     = "QTester.Group.Edit.Collapsing";
const char G_EDIT_TEXT[]           = "QTester.Group.Edit.Text";
const char G_EDIT_BLOCKS[]         = "QTester.Group.Edit.Blocks";
const char G_EDIT_FONT[]           = "QTester.Group.Edit.Font";
const char G_EDIT_EDITOR[]         = "QTester.Group.Edit.Editor";

const char G_TOOLS_OPTIONS[]       = "QTester.Group.Tools.Options";

// Window menu groups
const char G_WINDOW_SIZE[]         = "QTester.Group.Window.Size";
const char G_WINDOW_PANES[]        = "QTester.Group.Window.Panes";
const char G_WINDOW_VIEWS[]        = "QTester.Group.Window.Views";
const char G_WINDOW_SPLIT[]        = "QTester.Group.Window.Split";
const char G_WINDOW_NAVIGATE[]     = "QTester.Group.Window.Navigate";
const char G_WINDOW_LIST[]         = "QTester.Group.Window.List";
const char G_WINDOW_OTHER[]        = "QTester.Group.Window.Other";

// Help groups (global)
const char G_HELP_HELP[]           = "QTester.Group.Help.Help";
const char G_HELP_SUPPORT[]        = "QTester.Group.Help.Supprt";
const char G_HELP_ABOUT[]          = "QTester.Group.Help.About";
const char G_HELP_UPDATES[]        = "QTester.Group.Help.Updates";

const char WIZARD_CATEGORY_QT[] = "R.Qt";
const char WIZARD_TR_CATEGORY_QT[] = QT_TRANSLATE_NOOP("Core", "Qt");
const char WIZARD_KIND_UNKNOWN[] = "unknown";
const char WIZARD_KIND_PROJECT[] = "project";
const char WIZARD_KIND_FILE[] = "file";

const char SETTINGS_CATEGORY_CORE[] = "B.Core";
const char SETTINGS_CATEGORY_CORE_ICON[] = ":/core/images/category_core.png";
const char SETTINGS_TR_CATEGORY_CORE[] = QT_TRANSLATE_NOOP("Core", "Environment");
const char SETTINGS_ID_INTERFACE[] = "A.Interface";
const char SETTINGS_ID_SYSTEM[] = "B.Core.System";
const char SETTINGS_ID_SHORTCUTS[] = "C.Keyboard";
const char SETTINGS_ID_TOOLS[] = "D.ExternalTools";
const char SETTINGS_ID_MIMETYPES[] = "E.MimeTypes";

const char SETTINGS_DEFAULTTEXTENCODING[] = "General/DefaultFileEncoding";

const char SETTINGS_THEME[] = "Core/CreatorTheme";
const char DEFAULT_THEME[] = "flat";

const char TR_CLEAR_MENU[]         = QT_TRANSLATE_NOOP("Core", "Clear Menu");

const char DEFAULT_BUILD_DIRECTORY[] = "../%{JS: Util.asciify(\"build-%{CurrentProject:Name}-%{CurrentKit:FileSystemName}-%{CurrentBuild:Name}\")}";

const int MODEBAR_ICON_SIZE = 34;
const int DEFAULT_MAX_LINE_COUNT = 100000;

} // namespace Constants
} // namespace Core
