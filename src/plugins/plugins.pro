include(../../qtester.pri)

TEMPLATE  = subdirs

SUBDIRS   = \
    coreplugin


for(p, SUBDIRS) {
    PLUGIN_DEPENDS =
    include($$p/$${p}_dependencies.pri)
    pv = $${p}.depends
    $$pv = $$PLUGIN_DEPENDS
}
