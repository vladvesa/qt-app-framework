/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of Qt Creator.
**
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 as published by the Free Software
** Foundation with exceptions as appearing in the file LICENSE.GPL3-EXCEPT
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
****************************************************************************/

#pragma once

#include "icon.h"
#include "utils_global.h"

namespace Utils {
namespace Icons {

QTESTER_UTILS_EXPORT extern const Icon HOME;
QTESTER_UTILS_EXPORT extern const Icon HOME_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon EDIT_CLEAR;
QTESTER_UTILS_EXPORT extern const Icon EDIT_CLEAR_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon LOCKED_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon LOCKED;
QTESTER_UTILS_EXPORT extern const Icon UNLOCKED_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon NEXT;
QTESTER_UTILS_EXPORT extern const Icon NEXT_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon PREV;
QTESTER_UTILS_EXPORT extern const Icon PREV_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon PROJECT;
QTESTER_UTILS_EXPORT extern const Icon ZOOM;
QTESTER_UTILS_EXPORT extern const Icon ZOOM_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon ZOOMIN_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon ZOOMOUT_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon FITTOVIEW_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon OK;
QTESTER_UTILS_EXPORT extern const Icon NOTLOADED;
QTESTER_UTILS_EXPORT extern const Icon BROKEN;
QTESTER_UTILS_EXPORT extern const Icon BOOKMARK;
QTESTER_UTILS_EXPORT extern const Icon BOOKMARK_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon BOOKMARK_TEXTEDITOR;
QTESTER_UTILS_EXPORT extern const Icon SNAPSHOT_TOOLBAR;

QTESTER_UTILS_EXPORT extern const Icon NEWFILE;
QTESTER_UTILS_EXPORT extern const Icon OPENFILE;
QTESTER_UTILS_EXPORT extern const Icon OPENFILE_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon SAVEFILE;
QTESTER_UTILS_EXPORT extern const Icon SAVEFILE_TOOLBAR;

QTESTER_UTILS_EXPORT extern const Icon EXPORTFILE_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon MULTIEXPORTFILE_TOOLBAR;

QTESTER_UTILS_EXPORT extern const Icon UNDO;
QTESTER_UTILS_EXPORT extern const Icon UNDO_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon REDO;
QTESTER_UTILS_EXPORT extern const Icon REDO_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon COPY;
QTESTER_UTILS_EXPORT extern const Icon COPY_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon PASTE;
QTESTER_UTILS_EXPORT extern const Icon PASTE_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon CUT;
QTESTER_UTILS_EXPORT extern const Icon CUT_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon RESET;
QTESTER_UTILS_EXPORT extern const Icon RESET_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon DARK_CLOSE;

QTESTER_UTILS_EXPORT extern const Icon ARROW_UP;
QTESTER_UTILS_EXPORT extern const Icon ARROW_DOWN;
QTESTER_UTILS_EXPORT extern const Icon MINUS;
QTESTER_UTILS_EXPORT extern const Icon PLUS_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon PLUS;
QTESTER_UTILS_EXPORT extern const Icon MAGNIFIER;
QTESTER_UTILS_EXPORT extern const Icon CLEAN;
QTESTER_UTILS_EXPORT extern const Icon CLEAN_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon RELOAD;
QTESTER_UTILS_EXPORT extern const Icon TOGGLE_LEFT_SIDEBAR;
QTESTER_UTILS_EXPORT extern const Icon TOGGLE_LEFT_SIDEBAR_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon TOGGLE_RIGHT_SIDEBAR;
QTESTER_UTILS_EXPORT extern const Icon TOGGLE_RIGHT_SIDEBAR_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon CLOSE_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon CLOSE_FOREGROUND;
QTESTER_UTILS_EXPORT extern const Icon CLOSE_BACKGROUND;
QTESTER_UTILS_EXPORT extern const Icon SPLIT_HORIZONTAL;
QTESTER_UTILS_EXPORT extern const Icon SPLIT_HORIZONTAL_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon SPLIT_VERTICAL;
QTESTER_UTILS_EXPORT extern const Icon SPLIT_VERTICAL_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon CLOSE_SPLIT_TOP;
QTESTER_UTILS_EXPORT extern const Icon CLOSE_SPLIT_BOTTOM;
QTESTER_UTILS_EXPORT extern const Icon CLOSE_SPLIT_LEFT;
QTESTER_UTILS_EXPORT extern const Icon CLOSE_SPLIT_RIGHT;
QTESTER_UTILS_EXPORT extern const Icon FILTER;
QTESTER_UTILS_EXPORT extern const Icon LINK;

QTESTER_UTILS_EXPORT extern const Icon INFO;
QTESTER_UTILS_EXPORT extern const Icon INFO_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon WARNING;
QTESTER_UTILS_EXPORT extern const Icon WARNING_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon CRITICAL;
QTESTER_UTILS_EXPORT extern const Icon CRITICAL_TOOLBAR;

QTESTER_UTILS_EXPORT extern const Icon ERROR_TASKBAR;
QTESTER_UTILS_EXPORT extern const Icon EXPAND_ALL_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon TOOLBAR_EXTENSION;
QTESTER_UTILS_EXPORT extern const Icon RUN_SMALL;
QTESTER_UTILS_EXPORT extern const Icon RUN_SMALL_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon STOP_SMALL;
QTESTER_UTILS_EXPORT extern const Icon STOP_SMALL_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon INTERRUPT_SMALL;
QTESTER_UTILS_EXPORT extern const Icon INTERRUPT_SMALL_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon BOUNDING_RECT;
QTESTER_UTILS_EXPORT extern const Icon EYE_OPEN_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon EYE_CLOSED_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon REPLACE;
QTESTER_UTILS_EXPORT extern const Icon EXPAND;
QTESTER_UTILS_EXPORT extern const Icon EXPAND_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon COLLAPSE;
QTESTER_UTILS_EXPORT extern const Icon COLLAPSE_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon PAN_TOOLBAR;
QTESTER_UTILS_EXPORT extern const Icon EMPTY14;
QTESTER_UTILS_EXPORT extern const Icon EMPTY16;
QTESTER_UTILS_EXPORT extern const Icon OVERLAY_ADD;
QTESTER_UTILS_EXPORT extern const Icon OVERLAY_WARNING;
QTESTER_UTILS_EXPORT extern const Icon OVERLAY_ERROR;

QTESTER_UTILS_EXPORT extern const Icon CODEMODEL_ERROR;
QTESTER_UTILS_EXPORT extern const Icon CODEMODEL_WARNING;
QTESTER_UTILS_EXPORT extern const Icon CODEMODEL_DISABLED_ERROR;
QTESTER_UTILS_EXPORT extern const Icon CODEMODEL_DISABLED_WARNING;

} // namespace Icons
} // namespace Utils
