include(../../qtester.pri)

TEMPLATE  = subdirs

SUBDIRS   = aggregation \
            extensionsystem \
            utils


for(l, SUBDIRS) {
    LIB_DEPENDS =
    include($$l/$${l}_dependencies.pri)
    lv = $${l}.depends
    $$lv = $$LIB_DEPENDS
}
