#-------------------------------------------------
#
# Project created by QtCreator 2018-03-24T10:18:55
#
#-------------------------------------------------
include(../../qtester.pri)
include(../shared/qtsingleapplication/qtsingleapplication.pri)

TEMPLATE = app
TARGET = $$IDE_APP_TARGET
DESTDIR = $$IDE_APP_PATH
VERSION = $$QTESTER_VERSION
QT -= testlib

include(../rpath.pri)

LIBS *= -l$$libraryName(ExtensionSystem) -l$$libraryName(Aggregation) -l$$libraryName(Utils)


win32 {
    # We need the version in two separate formats for the .rc file
    #  RC_VERSION=4,3,82,0 (quadruple)
    #  RC_VERSION_STRING="4.4.0-beta1" (free text)
    # Also, we need to replace space with \x20 to be able to work with both rc and windres
    COPYRIGHT = "2008-$${QTESTER_COPYRIGHT_YEAR} The Qt Company Ltd"
    DEFINES += RC_VERSION=$$replace(QTESTER_VERSION, "\\.", ","),0 \
        RC_VERSION_STRING=\"$${QTESTER_DISPLAY_VERSION}\" \
        RC_COPYRIGHT=\"$$replace(COPYRIGHT, " ", "\\x20")\"
    RC_FILE = qtester.rc
} else:macx {
    LIBS += -framework CoreFoundation
    minQtVersion(5, 7, 1) {
        QMAKE_ASSET_CATALOGS = $$PWD/qtester.xcassets
        QMAKE_ASSET_CATALOGS_BUILD_PATH = $$IDE_DATA_PATH
        QMAKE_ASSET_CATALOGS_INSTALL_PATH = $$INSTALL_DATA_PATH
        QMAKE_ASSET_CATALOGS_APP_ICON = qtester
    } else {
        ASSETCATALOG.files = $$PWD/qtester.xcassets
        macx-xcode {
            QMAKE_BUNDLE_DATA += ASSETCATALOG
        } else {
            ASSETCATALOG.output = $$IDE_DATA_PATH/qtester.icns
            ASSETCATALOG.commands = xcrun actool \
                --app-icon qtester \
                --output-partial-info-plist $$shell_quote($(TMPDIR)/qtester.Info.plist) \
                --platform macosx \
                --minimum-deployment-target $$QMAKE_MACOSX_DEPLOYMENT_TARGET \
                --compile $$shell_quote($$IDE_DATA_PATH) \
                $$shell_quote($$PWD/qtester.xcassets) > /dev/null
            ASSETCATALOG.input = ASSETCATALOG.files
            ASSETCATALOG.CONFIG += no_link target_predeps
            QMAKE_EXTRA_COMPILERS += ASSETCATALOG
            icns.files = \
                $$IDE_DATA_PATH/qtester.icns \
                $$IDE_DATA_PATH/prifile.icns \
                $$IDE_DATA_PATH/profile.icns
            icns.path = $$INSTALL_DATA_PATH
            icns.CONFIG += no_check_exist
            INSTALLS += icns
        }
    }

    infoplist = $$cat($$PWD/app-Info.plist, blob)
    infoplist = $$replace(infoplist, @MACOSX_DEPLOYMENT_TARGET@, $$QMAKE_MACOSX_DEPLOYMENT_TARGET)
    infoplist = $$replace(infoplist, @QTESTER_COPYRIGHT_YEAR@, $$QTESTER_COPYRIGHT_YEAR)
    infoplist = $$replace(infoplist, @PRODUCT_BUNDLE_IDENTIFIER@, $$PRODUCT_BUNDLE_IDENTIFIER)
    write_file($$OUT_PWD/Info.plist, infoplist)

    QMAKE_INFO_PLIST = $$OUT_PWD/Info.plist
}


target.path = $$INSTALL_APP_PATH
INSTALLS += target

CONFIG += no_batch


DISTFILES += qtester.rc \
    Info.plist \
    $$PWD/app_version.h.in

QMAKE_SUBSTITUTES += $$PWD/app_version.h.in

SOURCES += \
        main.cpp

HEADERS +=

FORMS +=

