TEMPLATE = app
TARGET = qtester.sh

include(../qtester.pri)

OBJECTS_DIR =

PRE_TARGETDEPS = $$PWD/qtester.sh

QMAKE_LINK = cp $$PWD/qtester.sh $@ && : IGNORE REST OF LINE:
QMAKE_STRIP =
CONFIG -= qt separate_debug_info gdb_dwarf_index

QMAKE_CLEAN = qtester.sh

target.path  = $$INSTALL_BIN_PATH
INSTALLS    += target

DISTFILES = $$PWD/qtester.sh
